package com.example.numberconversion;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/*
 * Simple number conversion app to test Android features.
 * 
 * Issues: 
 * 1) Only a limited number of pre-defined bases
 * 2) No overflow checking (but using big ints)
 * 3) General lack of functionality (perhaps add fractions, adding/subtracting/multiplying)
 * 4) Very not pretty
 * 5) Possibly add settings to change history size
 * 6) Space efficiency
 */

public class MainActivity extends Activity {
	//Member variables
	private List<String> history = new LinkedList<String>();
	private int convert_from_base = 0;
	private int convert_to_base = 0;
	private Pattern pattern;
	private Matcher matcher;
	private int counter = 0;
	
	//Constants
	private static final int HISTORY_MAX_SIZE = 6;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Stop the keyboard form automatically popping up
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		//Initialize spinners with options
		//Initialize spinner to choose input base, final is used so we can change the selection
		final Spinner spinner = (Spinner) findViewById(R.id.convert_from_base_spinner);
		ArrayAdapter<CharSequence> adapter = 
				ArrayAdapter.createFromResource(this,R.array.convert_from_base_array,android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		
		//Spinner to choose what base to convert to
		final Spinner spinner2 = (Spinner) findViewById(R.id.convert_to_base_spinner);
		ArrayAdapter<CharSequence> adapter2 = 
				ArrayAdapter.createFromResource(this,R.array.convert_to_base_array,android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(adapter2);		
		spinner2.setSelection(BaseSelectors.BASE10.ordinal()); //Allow base 10
		
		//Make the display scroll-able if there is a text overflow
		TextView t = (TextView)findViewById(R.id.convert_history_box);
		t.setMovementMethod(new ScrollingMovementMethod());
		
		//Create a text listener that attempts to detect what base the input is
		//If the new key is higher than a base, we can change the base
		//Example: User inputs an 'f.'  'f' can only be in hex or higher.
		//However, this doesn't account for user typos
		EditText e = (EditText)findViewById(R.id.num_to_convert);
		e.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int count,
					int before) {
				if (s.length() < 1); //Do nothing if something like a soft key is pressed
				else if (s.charAt(s.length() - 1) >= 'a' || s.charAt(s.length() - 1) >= 'A')
					spinner.setSelection(BaseSelectors.BASE16.ordinal());
				else if (s.charAt(s.length() - 1) >= '8')
					spinner.setSelection(BaseSelectors.BASE10.ordinal());
			}

			@Override
			public void afterTextChanged(Editable s) {}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void convert(View view)
	{
		//Get necessary view elements
		TextView t = (TextView) findViewById(R.id.convert_history_box);
		Spinner s1 = (Spinner)findViewById(R.id.convert_from_base_spinner);
		Spinner s2 = (Spinner)findViewById(R.id.convert_to_base_spinner);
		EditText e = (EditText) findViewById(R.id.num_to_convert);
		String input = e.getText().toString().trim();
		
		//Split off the number part of the spinner text
		convert_from_base = Integer.parseInt(((s1.getSelectedItem().toString().split(" "))[0]));
		convert_to_base = Integer.parseInt(((s2.getSelectedItem().toString().split(" "))[0]));
		
		//Check corner cases and errors
		if (input.contentEquals("")) //Nothing was input
		{
			history.add("<font color='#EE0000'>You did not enter a number!</font>");
		}
		else if (convert_from_base == convert_to_base); //If base to and from are the same, do nothing.
		else
		{
			//Validate input, can be generalized with some ugly operations
			//if (base < 10, [0-base]+ otherwise...)
			if (convert_from_base == 2) pattern = Pattern.compile("(0|1)+");
			else if (convert_from_base == 8) pattern = Pattern.compile("[0-7]+");
			else if (convert_from_base == 10) pattern = Pattern.compile("[0-9]+");
			else pattern = Pattern.compile("[a-fA-F0-9]+");
			
			matcher = pattern.matcher(input);
			if (matcher.matches()) //VALID input, print the converted value
			{
				String base10 = (convert_to_decimal(input,convert_from_base)).toString();
				history.add(((counter % 2 == 0) ? "<font color='#f9dfcb'>" : "") + input + "(" + convert_from_base + ") ==> " 
						+ convert_from_decimal(base10,convert_to_base) + "(" + convert_to_base + ")" 
						+ ((counter % 2 == 0) ? "</font>":""));
				counter++;
			}
			else 
				history.add("<font color='#EE0000'>" +
						"You did not enter a valid base " + convert_from_base + " number!</font>");
		}
		
		//Cull the conversion history
		if (history.size() > HISTORY_MAX_SIZE) history.remove(0); //If too many elements, remove the first
		
		//Display the history
		String toDisplay = "";
		for (int i = 0; i < HISTORY_MAX_SIZE; i++)
		{
			if (i >= history.size()); //Don't go out of bounds...
			else
			{
				toDisplay += history.get(i) + "\n<br />";
			}
		}	
		t.setText(Html.fromHtml(toDisplay));
	}
	
	/*
	 * Function to convert an integer (in string form) to any base
	 */
	private BigInteger convert_to_decimal(String s, int base)
	{
		BigInteger val = new BigInteger("0");
		for (int i = 0; i < s.length(); i++)
		{
			BigInteger power = new BigInteger(Integer.toString(base));
			power = power.pow(s.length() - i - 1);
			BigInteger digit;
			if (s.charAt(i) >= 'a' && s.charAt(i) <= 'f')
				digit = new BigInteger(Integer.toString(s.charAt(i) - 'a' + 10));
			else if (s.charAt(i) >= 'A' && s.charAt(i) <= 'F')
				digit = new BigInteger(Integer.toString(s.charAt(i) - 'A' + 10));
			else
				digit = new BigInteger(s.substring(i,i+1));
			val = val.add(power.multiply(digit));
		}
		return val;
	}
	
	/*
	 * Function to convert an decimal (in string form) to a number of any base 
	 */
	private String convert_from_decimal(String s, int base)
	{
		String remainders = "";
		BigInteger val = new BigInteger(s);
		BigInteger zero = new BigInteger("0");
		BigInteger bi_base = new BigInteger(Integer.toString(base));
		String r = "";
		while (!val.divide(bi_base).equals(zero)) //at zero, stop dividing
		{
			r = val.mod(bi_base).toString();
			if (Integer.parseInt(r) < 10) remainders += r;
			else remainders += (char)('A' + (Integer.parseInt(r) - 10));
			val = val.divide(bi_base);
		}
		r = val.mod(bi_base).toString(); //Have to do it once more
		if (Integer.parseInt(r) < 10) remainders += r;
		else remainders += (char)('A' + (Integer.parseInt(r) - 10));
		
		//Don't forget to reverse the string
		String reverse = "";
		for (int i = remainders.length() - 1; i >= 0; i--)
		{
			reverse += remainders.charAt(i);
		}
		
		return reverse;
	}
}
